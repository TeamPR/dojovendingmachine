import React from "react";
import HomePage from "./home-page";

class App extends React.Component {
  render() {
    return (
      <div className="ui container" style={{ margin: "10px" }}>
        <HomePage onSubmit={this.onSearchSubmit} />
      </div>
    );
  }
}

export default App;
