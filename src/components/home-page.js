import React from "react";
import { Link } from 'react-router-dom';
import "../index.css";

class SearchBar extends React.Component {
  
  constructor(props) {
    super(props);
    this.products = [];
    this.state = {
      products: []
    };
    this.loading = false;
  }
  /**
   * It will fetch all products from mongoDB api
   */
  getProducts() {
    const key = "IGjlk6B0qSjvj_EAnewz3cHxAbTpYY7v";
    const url = `https://api.mlab.com/api/1/databases/vending_machine/collections/products?apiKey=${key}`;

    fetch(url, {
      method: 'GET'
    }).then(response => response.text())
    .then(data => {
      this.setState({ products: JSON.parse(data)});
      console.log(this.state.products);
    })
  }
  /**
   * It will get rendered after component has been mounted
   */
  componentDidMount() {
    console.log("component did mount");
    this.getProducts();
    
  }

  render() {
    return (
      <div className="outer-grid">
          <h1 className="title">
                Vending Machine
            </h1>
          <div className="ui grid container">
          {
            this.state.products.length > 0 ?
              this.state.products.map((product, index) => {
                  return(
                    <div key={index} className="four wide column">
                      <div className="ui card">
                        <div className="image">
                          <img className="img-main" src="images/choco.jpg" alt="" />
                        </div>
                        <div className="content">
                          <Link className="header" to={`/insert-money/${product._id.$oid}`}>{product.name}</Link>
                          <div className="meta">
                              <span className="date">Price $ {product.price}</span>
                          </div>
                          <div className="description">
                            {product.description}
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                })
            :
              <div></div>
          }            
        </div>
      </div>
      
    );
  }
}

export default SearchBar;
