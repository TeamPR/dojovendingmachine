import React from "react";
import "../index.css";

class InsertMoney extends React.Component {
  
  constructor(props) {
    super(props);
    this.products = [];
    this.state = {
      product: {},
      return: 0,
      enter: 0
    };
    this.loading = false;
  }
  /**
   * It will fetch all products from mongoDB api
   */
  getProduct(id) {
    const key = "IGjlk6B0qSjvj_EAnewz3cHxAbTpYY7v";
    const url = `https://api.mlab.com/api/1/databases/vending_machine/collections/products/${id}?apiKey=${key}`;

    fetch(url, {
      method: 'GET'
    }).then(response => response.text())
    .then(data => {
      this.setState({ product: JSON.parse(data)});
      console.log(this.state.product);
    })
  }
  /**
   * It will get rendered after component has been mounted
   */
  componentDidMount() {
    let { id } = this.props.match.params;
    this.getProduct(id);
    console.log(id);
  }

  sum = (event) => {
        var returnVal;
        var  sumAmount = (this.state.enter + parseFloat(event.target.innerHTML));
        this.setState({enter: sumAmount});
        returnVal =  (sumAmount - this.state.product.price).toFixed(2);
        returnVal = (returnVal < 0) ? 0 : returnVal;
        this.setState({return: returnVal});
  }

  render() {
    return (
        <div id="insert-card" className="ui card main text container">
            <h1 className="title">
                Insert Money
            </h1>
            {/* <div className="img">
                <img alt="Image" src="https://semantic-ui.com/images/avatar2/large/kristy.png" />
            </div> */}
            <div className="content">
                <a href="index.html" className="header">{this.state.product.name}</a>
                <div className="meta">
                    <span className="date">Price $ {this.state.product.price}</span>
                </div>
                <div className="description">
                    {this.state.product.description}
                </div>
                
                <div className="description">
                        
                            <button onClick={ this.sum } className="ui inverted blue button">0.5</button>
                        
                        
                            <button onClick={ this.sum } className="ui inverted blue button">0.10</button>
                        
                        
                            <button onClick={ this.sum } className="ui inverted blue button">0.25</button>
                        
                        
                            <button onClick={ this.sum } className="ui inverted blue button">1.00</button>
                        
                </div>
                <div className="description">
                    <div className="label">
                        Enter Money
                    </div>
                    <div className="ui input">                        
                        <div class="ui label">{`$ ${this.state.enter}`}</div>
                    </div>
                </div>  
                <div className="description">
                    <div className="label">
                        Item Cost
                    </div>
                    <div className="ui input">
                        <div class="ui label">{`$ ${this.state.product.price}`}</div> 
                    </div>
                </div>
                <div className="description">
                    <div className="label">
                        Return Money
                    </div>
                    <div className="ui input">
                        <div class="ui label">{`$ ${this.state.return}`}</div>
                    </div>
                </div>
                <div className="description">
                    <div className="ui vertical animated button" tabindex="0">
                    <div className="hidden content">Shop</div>
                    <div className="visible content">
                        <i className="shop icon"></i>
                    </div>
                </div>
                <button className="ui button">
                Discard
                </button>
                </div>
            </div>    
        </div>
    );
  }
}

export default InsertMoney;
