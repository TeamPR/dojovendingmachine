import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import InsertMoney from './components/insert-money';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

ReactDOM.render(<BrowserRouter>
                    <Switch>
                        <Route exact path='/' component={App} />
                        <Route exact path='/insert-money/:id' component={InsertMoney} />
                    </Switch>
                </BrowserRouter>,
     document.querySelector("#root"));
