package util;

import controllers.PageObjectController;
import controllers.WebDriverManager;

public class sharedTestContext {
	private WebDriverManager webDriverManager;
	private PageObjectController pageObjectManager;

	public sharedTestContext(){
		webDriverManager = new WebDriverManager();
		pageObjectManager = new PageObjectController(webDriverManager.getDriver());
	}

	public WebDriverManager getWebDriverManager() {
		return webDriverManager;
	}

	public PageObjectController getPageObjectManager() {
		return pageObjectManager;
	}

}