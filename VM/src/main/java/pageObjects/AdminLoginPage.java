package pageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.junit.Assert;

import controllers.FileReaderManager;

public class AdminLoginPage {
	WebDriver driver;
	String username;
	String passowrd;

	public AdminLoginPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}


	 @FindBy(how = How.XPATH, using = "//input[@name='email']") 
	 private WebElement emailEditbox;

	 @FindBy(how = How.XPATH, using = "//input[@name='password']") 
	 private WebElement passpwrdEditbox;

	 @FindBy(how = How.XPATH, using = "//a[text()='Login']") 
	 private WebElement loginButton;

	 @FindBy(how = How.XPATH, using = "//h1[text()='Admin Page']") 
	 private WebElement pageHeaderTitle;


	 public void navigateTo_homePage() {	
		driver.navigate().to(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());
		}
	 
	 public void validateLoginOption() {	
		navigateTo_homePage();
		emailEditbox.isDisplayed();
		passpwrdEditbox.isDisplayed();
		Assert.assertTrue("login widget displayed", loginButton.isDisplayed());
		}
	 
	 public boolean login(String userName, String pwd) {	
		this.username = userName;
		this.passowrd = pwd;
		navigateTo_homePage();
		emailEditbox.sendKeys(FileReaderManager.getInstance().getConfigReader().getUsername());
		passpwrdEditbox.sendKeys(FileReaderManager.getInstance().getConfigReader().getPwd());
		loginButton.click();
		return true;
	}

	public void loginState(boolean expectedState) {
		Assert.assertEquals(pageHeaderTitle.getText().equalsIgnoreCase("Replainishment options"), expectedState);
	}
	

}