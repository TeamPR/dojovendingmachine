package controllers;

import org.openqa.selenium.WebDriver;
import pageObjects.AdminReplainishPage;
import pageObjects.AdminLoginPage;


public class PageObjectController {

	private WebDriver driver;
	private AdminLoginPage homePage;
	private AdminReplainishPage checkoutPage;

	public PageObjectController(WebDriver driver) {
		this.driver = driver;
	}

	public AdminLoginPage getHomePage(){
		return (homePage == null) ? homePage = new AdminLoginPage(driver) : homePage;
	}

	public AdminReplainishPage getCheckoutPage() {

		return (checkoutPage == null) ? checkoutPage = new AdminReplainishPage(driver) : checkoutPage;

	}
}