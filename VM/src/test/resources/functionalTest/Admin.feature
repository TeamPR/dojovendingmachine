#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template


@smokeTest
Feature: To test admin feature for Vending machine
This is to test all admin features

Scenario: Basic maintinance flow

Given Vending Machine is in Ready State
When Service User has accessed the Vending Machine
Then the Vending Machine should have a option for Admin login with username and password


Scenario: Basic maintinance login positive flow 

Given Vending Machine is in Ready State
When insert valid login username and password
Then Vending Machine should display the admin screen with correct option


Scenario: Basic maintinance login negative flow 

Given Vending Machine is in Ready State
When insert invalid login username and password
Then Vending Machine should display the error message