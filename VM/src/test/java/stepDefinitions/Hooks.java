package stepDefinitions;

import util.sharedTestContext;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	sharedTestContext testContext;

	public Hooks(sharedTestContext context) {
		testContext = context;
	}

	@Before
	public void BeforeSteps() {

	}

	@After
	public void AfterSteps() {
		testContext.getWebDriverManager().quitDriver();
	}

}