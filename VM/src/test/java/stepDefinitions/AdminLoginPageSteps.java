package stepDefinitions;

import org.openqa.selenium.WebDriver;

import controllers.FileReaderManager;
import controllers.PageObjectController;
import controllers.WebDriverManager;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.AdminLoginPage;

public class AdminLoginPageSteps {
	WebDriver driver = WebDriverManager.driver;
	AdminLoginPage homePage;
	PageObjectController pageObjectManager; 
	//WebDriverManager webDriverManager;


	@Given("^Vending Machine is in Ready State$")
	public void vending_Machine_is_in_Ready_State() throws Throwable {
		//webDriverManager = new WebDriverManager();
		//driver = webDriverManager.getDriver();
		pageObjectManager = new PageObjectController(driver);
		homePage = pageObjectManager.getHomePage();
		homePage.navigateTo_homePage(); 
	}

	@When("^Service User has accessed the Vending Machine$")
	public void service_User_has_accessed_the_Vending_Machine() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException("OutofScope for this release");
	}

	@Then("^the Vending Machine should have a option for Admin login with username and password$")
	public void the_Vending_Machine_should_have_a_option_for_Admin_login_with_username_and_password() throws Throwable {
		homePage.validateLoginOption();
	}

	@When("^insert valid login username and password$")
	public void insert_valid_login_username_and_password() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		homePage.login(FileReaderManager.getInstance().getConfigReader().getUsername(), FileReaderManager.getInstance().getConfigReader().getPwd());
	}

	@Then("^Vending Machine should display the admin screen with correct option$")
	public void vending_Machine_should_display_the_admin_screen_with_correct_option() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    boolean state = true;
		homePage.loginState(state);
	}

	@When("^insert invalid login username and password$")
	public void insert_invalid_login_username_and_password() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		homePage.login("prasrawat", "1234");
	}

	@Then("^Vending Machine should display the error message$")
	public void vending_Machine_should_display_the_error_message() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException("OutofScope for this release");
	}

}